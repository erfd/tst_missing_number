package org.fbnv.hw20230826;

import junit.framework.TestCase;

public class TestLowestMissingNumber extends TestCase {
	public TestLowestMissingNumber(String testName) {
		super(testName);
	}

	public void testEmptyArray() {
		testGetLowestMissing(null);
	}

	public void testOnlyElementArray() {
		testGetLowestMissing(null, 99);
	}

	public void testOnlyMissing() {
		testGetLowestMissing(1, 2, 0);
	}

	public void testTwoMissing() {
		testGetLowestMissing(1, 5, 0, 4, 2);
	}

	public void testLongRangeHole() {
		testGetLowestMissing(36, 1000, 200, 555, 35);
	}

	public void testNoMissing() {
		testGetLowestMissing(null, 7, 5, 9, 6, 8);
	}


	private void testGetLowestMissing(Integer expected, Integer... elements) {
		assertEquals(expected, App.getLowestMissing(elements));
	}
}
