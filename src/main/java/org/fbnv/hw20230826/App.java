package org.fbnv.hw20230826;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class App {
	public static void main(String[] args) {
	}

	public static Integer getLowestMissing(Integer[] array) {
		// Нужна сортировка, поэтому используем TreeSet, чтобы не пришлось позже явно сортировать элементы.
		Set<Integer> set = new TreeSet<>(Arrays.asList(array));
		Integer previous = null;
		for (Integer element : set) {
			// Если элементов нет или элемент один, то сравнивать ничего не нужно, сразу ответим null.
			if (previous != null) {
				// Начиная со второго элемента уже нужно сравнивать разницу между числами.
				if (previous + 1 != element) {
					// Двигаясь по отсортированному массиву мы ожидаем элементы по порядку.
					// Если это не так, значит мы нашли первый недостающий элемент.
					return previous + 1;
				}
			}
			previous = element;
		}
		// Если все элементы будут по порядку или элементов меньше 2, то вернём null.
		return null;
	}
}
